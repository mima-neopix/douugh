<?php
/**
 * The Header for our theme.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */
?><!DOCTYPE html>
<?php 
	if( $_GET && key_exists('mfn-rtl', $_GET) ):
		echo '<html class="no-js" lang="ar" dir="rtl">';
	else:
?>
<html class="no-js" <?php language_attributes(); ?><?php mfn_tag_schema(); ?>>
<?php endif; ?>

<!-- head -->
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
        $(document).ready(function(){

            $('#popup_form .close_btn, #popupBack').click(function(e){
                e.preventDefault();

                $('#popup_form').fadeOut();
            });
            $('#menu-douugh_menu .get-informed-btn').click(function(e){
                e.preventDefault();
                $('#popup_form').css("display","block");
            });
            $('.menu li').hover(function(e){
            	e.preventDefault();
                $('.get-informed-btn').removeClass("hover");
            });
            $('#popup_success .close_btn, #popupBack').click(function(e){
                e.preventDefault();

                $('#popup_success').fadeOut();
            });
            $('#popup_form .popup-form .submit-btn').click(function(e){
                e.preventDefault();
                $('#popup_form').css("display","none");
                $('#popup_success').css("display","block");
            });
            $('input').on('focus', function(){
			  	$('.contact-us-form label').addClass('focused-label');
			});
            $('input').on('blur', function(){
			  	$('.contact-us-form label').removeClass('focused-label');
			});
			$('textarea').on('focus', function(){
			  	$('.contact-us-form label').addClass('focused-label');
			});
            $('textarea').on('blur', function(){
			  	$('.contact-us-form label').removeClass('focused-label');
			});
			$('#Subheader .container .column').append(
			  	$('<div/>')
			    	.addClass('blog-cover-title')
			     	.text("Blog")
				);
			$('#Subheader .container .column').append(
			  	$('<img src="http://neopixdev.com/douugh/wp-content/uploads/2016/07/white-divider.png">')
			    	.addClass('blog-cover-image')
				);
			$('#Subheader .container .column').append(
			  	$('<div/>')
			    	.addClass('blog-cover-subtitle')
			     	.text("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa cum sociis natoque. ")
				);
			$('.widget_archive h3').append(
			  	$('<img src="http://neopixdev.com/douugh/wp-content/uploads/2016/07/purple-divider.png">')
			    	.addClass('widget-archive-divider') 
				);
			
			$('.widget_mfn_recent_posts h3').append(
			  	$('<img src="http://neopixdev.com/douugh/wp-content/uploads/2016/07/green-divider.png">')
			    	.addClass('widget-recentpost-divider') 
				);
			$('.field').attr('placeholder', 'Search').val('').focus().blur(); 
        });

</script>
<style>
    .popup_container {
    	width: 780px;
		height: 450px;
		box-shadow: 0px 0px 11px 4px rgba(0, 0, 0, 0.25) !important;
		display: block;
		top: 50%;
		left: 50%;
		transform: translate(-50%,-50%);
		position: absolute;
		padding: 10px;
		background-color: #ffffff;
		border-radius: 10px;
		margin-left: -400px;
		margin-top: -235px;
    }
    #popup_form {
        height: 100%;
        position: fixed;
        width: 100%;
        z-index: 30000;
        top: 0px;
        display: none;
    }
    #popup_success {
        height: 100%;
        position: fixed;
        width: 100%;
        z-index: 30000;
        top: 0px;
        display: none;
    }
    #popupBack {
        height: 100%;
        width: 100%;
        z-index: 10000;
        background-color: #4d4d4d;
		opacity: 0.75;
    }
    .popup_container h2 {
    	text-align:center; 
    	font-size:48px; 
    	font-family: "Akkurat Light"; 
    	color:#444444;
    	letter-spacing: -0.25px;
    	margin-top: 55px;
    }
    .popup_container p {
    	text-align: center;
    	font-size: 19px;
    	padding: 0 40px;
    	margin-top: 30px;
		margin-bottom: 65px;
    }        
    .close_btn {
        position:absolute;
        top: 8px;
        right: 8px;
    }
    .popup-form { 
	    width: 560px; 
	    text-align: center; 
	    display: block;
	    margin: 0 auto;
	}
	.popup-form input { width: 100% !important; }
	.popup-form label {
	    font-family: "Akkurat";
	    font-size: 17px;
	    text-align: left;
	}
	.popup-form input#email {
	    background-color: transparent;
	    border: none;
	    border-bottom: 3px solid #dddddd;
	    position: relative;
	    bottom: 35px;
	    border-radius: 2px;
	    box-shadow: unset;
	    transition: all 0.5s ease; 
	}
	.popup-form input#email:focus {
	    background-color: transparent !important;
	    color: #4d4d4d;
	    font-size: 19px;
	    font-family: "Akkurat";
	    bottom: 15px;
	    transition: all 0.5s ease;
	    border-image: -moz-linear-gradient( 0deg, rgb(0,161,134) 0%, rgb(0,245,161) 100%) !important;
	    border-image: -webkit-linear-gradient( 0deg, rgb(0,161,134) 0%, rgb(0,245,161) 100%);
	    border-image: -ms-linear-gradient( 0deg, rgb(0,161,134) 0%, rgb(0,245,161) 100%);
	    box-shadow: 0px 15px 10px -10px rgba(10, 7, 11, 0.15);
	}
	.popup-form input#submit-btn {
	    border-radius: 25px;
	    position: relative;
	    height: 50px;
	    right: 5px;
	    font-size: 17px;
	    font-family: "Akkurat";
	    letter-spacing: 0.25px;
	    background-image: -moz-linear-gradient( 82deg, rgb(6,116,235) 0%, rgb(4,163,239) 100%);
	    background-image: -webkit-linear-gradient( 82deg, rgb(6,116,235) 0%, rgb(4,163,239) 100%);
	    background-image: -ms-linear-gradient( 82deg, rgb(6,116,235) 0%, rgb(4,163,239) 100%);
	}
	#popup_success .popup_container > img {
	    display: block;
	    margin: 0px auto;
	    padding-top: 75px;
	}

</style>

<!-- meta -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php 
	if( mfn_opts_get('responsive') ){
		if( mfn_opts_get('responsive-zoom') ){
			echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
		} else {
			echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
		}
		 
	}
?>

<?php do_action('wp_seo'); ?>

<link rel="shortcut icon" href="<?php mfn_opts_show( 'favicon-img', THEME_URI .'/images/favicon.ico' ); ?>" />	
<?php if( mfn_opts_get('apple-touch-icon') ): ?>
<link rel="apple-touch-icon" href="<?php mfn_opts_show( 'apple-touch-icon' ); ?>" />
<?php endif; ?>	

<!-- wp_head() -->
<?php wp_head(); ?>
</head>

<!-- body -->
<body <?php body_class(); ?>>
	
	<?php do_action( 'mfn_hook_top' ); ?>
	
	<?php get_template_part( 'includes/header', 'sliding-area' ); ?>
	
	<?php if( mfn_header_style( true ) == 'header-creative' ) get_template_part( 'includes/header', 'creative' ); ?>

	<div id="popup_form">
        <div id="popupBack"></div>
        <div class="popup_container animate zoomIn" data-anim-type="zoomIn">
            <a class="close_btn" href="#"><img src="http://neopixdev.com/douugh/wp-content/uploads/2016/07/X-btn.png"></a>
            <h2>Don’t miss the next big thing.</h2>
            <p class="section-content">
            	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque.
            </p>
            <form action="" method="post" class="popup-form">
            	<label>Your Email</label>
                <input id="email" type="text" name="email" placeholder="" value="">
                <input id="submit-btn" class="submit-btn" type="submit" value="SUBSCRIBE">
            </form>
        </div>
    </div>
    <div id="popup_success">
        <div id="popupBack"></div>
        <div class="popup_container animate zoomIn" data-anim-type="zoomIn">
            <a class="close_btn" href="#"><img src="http://neopixdev.com/douugh/wp-content/uploads/2016/07/X-btn.png"></a>
            <img src="http://neopixdev.com/douugh/wp-content/uploads/2016/07/checkmark-icon.png">
            <h2>Success!</h2>
            <p class="section-content">
            	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque.
            </p>
        </div>
    </div>
	
	<!-- #Wrapper -->
	<div id="Wrapper">
	
		<?php 
			// Header Featured Image ----------
			$header_style = '';
			
			// Image -----
			if( mfn_ID() && ! is_search() ){
				
				if( ( ( mfn_ID() == get_option( 'page_for_posts' ) ) || ( get_post_type( mfn_ID() ) == 'page' ) ) && has_post_thumbnail( mfn_ID() ) ){

					// Pages & Blog Page ---
					$subheader_image = wp_get_attachment_image_src( get_post_thumbnail_id( mfn_ID() ), 'full' );
					$header_style .= ' style="background-image:url('. $subheader_image[0] .');"';

				} elseif( get_post_meta( mfn_ID(), 'mfn-post-header-bg', true ) ){

					// Single Post ---
					$header_style .= ' style="background-image:url('. get_post_meta( mfn_ID(), 'mfn-post-header-bg', true ) .');"';

				}
			}
			
			// Attachment -----
			if( mfn_opts_get('img-subheader-attachment') == 'fixed' ){
				
				$header_style .= ' class="bg-fixed"';
				
			} elseif( mfn_opts_get('img-subheader-attachment') == 'parallax' ){
				
				if( mfn_opts_get( 'parallax' ) == 'stellar' ){
					$header_style .= ' class="bg-parallax" data-stellar-background-ratio="0.5"';
				} else {
					$header_style .= ' class="bg-parallax" data-enllax-ratio="0.3"';
				}
				
			}
		?>
		
		<?php if( mfn_header_style( true ) == 'header-below' ) echo mfn_slider(); ?>

		<!-- #Header_bg -->
		<div id="Header_wrapper" <?php echo $header_style; ?>>
	
			<!-- #Header -->
			<header id="Header">
				<?php if( mfn_header_style( true ) != 'header-creative' ) get_template_part( 'includes/header', 'top-area' ); ?>	
				<?php if( mfn_header_style( true ) != 'header-below' ) echo mfn_slider(); ?>
			</header>
				
			<?php 
				if( ( mfn_opts_get('subheader') != 'all' ) && 
					( ! get_post_meta( mfn_ID(), 'mfn-post-hide-title', true ) ) &&
					( get_post_meta( mfn_ID(), 'mfn-post-template', true ) != 'intro' )	){

					
					$subheader_advanced = mfn_opts_get( 'subheader-advanced' );
					
					$subheader_style = '';
					
					if( mfn_opts_get( 'subheader-padding' ) ){
						$subheader_style .= 'padding:'. mfn_opts_get( 'subheader-padding' ) .';';
					}				
					
					
					if( is_search() ){
						// Page title -------------------------
						
						echo '<div id="Subheader" style="'. $subheader_style .'">';
							echo '<div class="container">';
								echo '<div class="column one">';

									if( trim( $_GET['s'] ) ){
										global $wp_query;
										$total_results = $wp_query->found_posts;
									} else {
										$total_results = 0;
									}

									$translate['search-results'] = mfn_opts_get('translate') ? mfn_opts_get('translate-search-results','results found for:') : __('results found for:','betheme');								
									echo '<h1 class="title">'. $total_results .' '. $translate['search-results'] .' '. esc_html( $_GET['s'] ) .'</h1>';
									
								echo '</div>';
							echo '</div>';
						echo '</div>';
						
						
					} elseif( ! mfn_slider_isset() || ( is_array( $subheader_advanced ) && isset( $subheader_advanced['slider-show'] ) ) ){
						// Page title -------------------------
						
						
						// Subheader | Options
						$subheader_options = mfn_opts_get( 'subheader' );


						if( is_home() && ! get_option( 'page_for_posts' ) && ! mfn_opts_get( 'blog-page' ) ){
							$subheader_show = false;
						} elseif( is_array( $subheader_options ) && isset( $subheader_options['hide-subheader'] ) ){
							$subheader_show = false;
						} elseif( get_post_meta( mfn_ID(), 'mfn-post-hide-title', true ) ){
							$subheader_show = false;
						} else {
							$subheader_show = true;
						}
						
						if( is_array( $subheader_options ) && isset( $subheader_options['hide-breadcrumbs'] ) ){
							$breadcrumbs_show = false;
						} else {
							$breadcrumbs_show = true;
						}
						
						
						if( is_array( $subheader_advanced ) && isset( $subheader_advanced['breadcrumbs-link'] ) ){
							$breadcrumbs_link = 'has-link';
						} else {
							$breadcrumbs_link = 'no-link';
						}
						
						
						// Subheader | Print
						if( $subheader_show ){
							echo '<div id="Subheader" style="'. $subheader_style .'">';
								echo '<div class="container">';
									echo '<div class="column one">';
										
										// Title
										$title_tag = mfn_opts_get( 'subheader-title-tag', 'h1' );
										echo '<'. $title_tag .' class="title">'. mfn_page_title() .'</'. $title_tag .'>';
										
										// Breadcrumbs
										if( $breadcrumbs_show ) mfn_breadcrumbs( $breadcrumbs_link );
										
									echo '</div>';
								echo '</div>';
							echo '</div>';
						}
						
					}
					
					
				}
			?>
		
		</div>
		
		<?php 
			// Single Post | Template: Intro
			if( get_post_meta( mfn_ID(), 'mfn-post-template', true ) == 'intro' ){
				get_template_part( 'includes/header', 'single-intro' );
			}
		?>
		
		<?php do_action( 'mfn_hook_content_before' );
		
// Omit Closing PHP Tags
